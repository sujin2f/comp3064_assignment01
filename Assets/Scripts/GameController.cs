﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	// UI
	[SerializeField]
	GameObject lifeImage;
	[SerializeField]
	Text scoreLabel;
	[SerializeField]
	Image gameOverImage;
	[SerializeField]
	Image hitEffect;
	[SerializeField]
	Text highScoreLabel;
	[SerializeField]
	Button resetBtn;

	// Num of Life
	[SerializeField]
	int life;

	// Screen Bound
	[SerializeField]
	public float boundX = 8;
	[SerializeField]
	public float boundY = 5;

	// Enemy Settings
	[SerializeField]
	private GameObject enemy;
	[SerializeField]
	private float spawnTime = 1f;
	[SerializeField]
	private int maxEnemy = 10;

	private void Initialize(){
		// initialize the score and life
		Player.Instance.Score = 0;
		Player.Instance.Life = life;
		SetLife (life);

		// hide/show texts/images
		gameOverImage.gameObject.SetActive (false);
		highScoreLabel.gameObject.SetActive (false);
		resetBtn.gameObject.SetActive (false);
		hitEffect.gameObject.SetActive (false);
		scoreLabel.gameObject.SetActive (true);

		// spawn enemies
		InvokeRepeating ("SpawnEnemy", spawnTime, spawnTime);
	}

	// Gameover Action
	public void GameOver(){
		// change high score text
		highScoreLabel.text = "HIGH SCORE : " + Player.Instance.HighScore;

		// hide/show texts/images
		gameOverImage.gameObject.SetActive (true);
		highScoreLabel.gameObject.SetActive (true);
		resetBtn.gameObject.SetActive (true);
		scoreLabel.gameObject.SetActive (false);
	}

	// Change Score
	public void UpdateUI(){
		scoreLabel.text = Player.Instance.Score.ToString();
	}

	// Use this for initialization
	void Start () {
		Player.Instance.gCtrl = this;
		Initialize ();
	}

	// Reset button callback
	public void ResetBtnClick(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	// Show red background
	// cite : https://docs.unity3d.com/ScriptReference/WaitForSeconds.html
	public IEnumerator ShowHitEffect() {
		hitEffect.gameObject.SetActive (true);
		yield return new WaitForSeconds(0.2f);
		hitEffect.gameObject.SetActive (false);
	}

	// Set/ Reset life icon
	public void SetLife(int life) {
		GameObject[] lifes = GameObject.FindGameObjectsWithTag ("Life");
		Transform spawnPoints = new GameObject().transform;

		if (lifes.Length == 0) {
			for(int i = 0; i < life + 1; i++) {
				Vector2 _currentPos = new Vector2 ((1 * i) - 5.5f, 4f);
				Instantiate (lifeImage, _currentPos, spawnPoints.rotation);
			}

		} else {
			DestroyObject (lifes [lifes.Length - 1]);
		}
	}

	// Spawn enemy
	void SpawnEnemy () {
		if (Player.Instance.NumEnemy >= maxEnemy)
			return;

		// Spawn! and increase number
		Instantiate (enemy);
		Player.Instance.NumEnemy++;
	}
}
