﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Spaceship Controller
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls spaceship by user
 */

public class SpaceshipController : MonoBehaviour {
	// Public variables
	[SerializeField]
	private GameObject laser;

	[SerializeField]
	private float speed = 0.15f;

	// Private variables
	private float topY;
	private float bottomY;
	private float boundY = 0.9f;

	private Transform _transform;
	private Vector2 _currentPos;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Set bound from setting
		topY = Player.Instance.gCtrl.boundY - boundY;
		bottomY = topY * -1;
	}
	
	// Update is called once per frame
	void Update () {
		UpdatePotition ();
		FireLaser ();
	}

	private void UpdatePotition() {
		_currentPos = _transform.position;

		float userInput = Input.GetAxis ("Vertical");

		if(userInput > 0) { 
			// Move up
			_currentPos += new Vector2 (0, speed);
		} else if (userInput < 0) { 
			// Move down
			_currentPos -= new Vector2 (0, speed);
		}

		CheckBounds ();

		// Apply the position
		_transform.position = _currentPos;
	}

	private void FireLaser() {
		if (Input.GetKeyDown ("space") || Input.GetKeyDown ("j")) {
			// Create Laser
			Instantiate (laser, _transform.position, _transform.rotation);
		}
	}

	// Check y bound
	private void CheckBounds(){
		if (_currentPos.y < bottomY) {
			// Limit bottom
			_currentPos.y = bottomY;
		} else if (_currentPos.y > topY) {
			// Limit top
			_currentPos.y = topY;
		}
	}
}
