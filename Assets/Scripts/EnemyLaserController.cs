﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Laser Contoller
 * @author      Sujin Byun
 * @created     2017-10-16
 * @modified    2017-10-16
 * @description Controls enemy laser
 */

public class EnemyLaserController : MonoBehaviour {
	// Public variables
	[SerializeField]
	private float speed = 0.05f;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	private float endX;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		endX = (Player.Instance.gCtrl.boundX + 2) * -1;
	}
	
	// Update is called once per frame
	void Update () {
		_currentPos = _transform.position;
		_currentPos -= new Vector2 (speed, 0);
		_transform.position = _currentPos;

		// Destroy this when it goes to out of bound
		if (_currentPos.x <= endX)
			DestroyObject (gameObject);
	}
}
