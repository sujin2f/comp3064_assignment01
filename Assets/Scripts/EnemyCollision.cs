﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Collision
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls enemy-laser collision
 */

public class EnemyCollision : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;

	public void OnTriggerEnter2D(Collider2D other) {
		// Hits by laser
		if (other.gameObject.tag.Equals("laser")) {
			// Prevent to destroied before it appears
			if (!gameObject.GetComponent<EnemyController> ().IsReady)
				return;

			// Decrease Num Enemies
			Player.Instance.NumEnemy--;

			// Create Explosion
			Vector2 _currentPos = new Vector2 (other.gameObject.transform.position.x + 2, other.gameObject.transform.position.y);
			Instantiate (explosion, _currentPos, other.gameObject.transform.rotation);

			// Increase Score
			Player.Instance.Score += 10;

			// Delete Objects (Enemy and Laser)
			DestroyObject (gameObject);
			DestroyObject (other);
		}
	}
}
