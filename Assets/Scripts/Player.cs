﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Player
 * @author      Sujin Byun
 * @created     2017-10-16
 * @modified    2017-10-16
 * @description Singleton class to store game data
 */

public class Player {
	// Instance
	static private Player _instance;
	static public Player Instance{
		get { 
			if (_instance == null)
				_instance = new Player ();
			return _instance;
		}
	}
	private Player() {}

	public GameController gCtrl;

	// variables and getter/setter
	private int _score = 0;
	private int _highScore = 0;
	private int _life = 0;
	private int _numEnemy = 0;

	public int Score{
		get{ return _score; }
		set{ 
			_score = value;
			if (_score > _highScore)
				_highScore = _score;
			
			gCtrl.UpdateUI();
		}
	}

	public int HighScore {
		get{ return _highScore; }
	}

	public int Life{
		get{ return _life; }
		set{ 
			_life = value;
			gCtrl.SetLife (_life);

			if (_life <= 0) {
				//game over
				gCtrl.GameOver();
			}else{
				gCtrl.UpdateUI();
			}
		}
	}

	public int NumEnemy{
		get{ return _numEnemy; }
		set{ _numEnemy = value; }
	}
}
