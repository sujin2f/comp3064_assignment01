﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Spaceship Collision Contoller
 * @author      Sujin Byun
 * @created     2017-10-16
 * @modified    2017-10-16
 * @description Controls collision for spaceship
 */

public class SpaceshipCollision : MonoBehaviour {
	// Explosion Animation
	[SerializeField]
	private GameObject explosion;

	// Collision occured
	public void OnTriggerEnter2D(Collider2D other) {
		// Hits by enemy laser
		if (other.gameObject.tag.Equals("EnemyLaser")) {
			// Create Explosion at the right position
			Vector2 _currentPos = new Vector2 (other.gameObject.transform.position.x - 1.5f, other.gameObject.transform.position.y);
			Instantiate (explosion, _currentPos, other.gameObject.transform.rotation);

			// Descrese Life
			Player.Instance.Life--;

			// Show hit effect (red background)
			StartCoroutine(Player.Instance.gCtrl.ShowHitEffect());

			DestroyObject (other);

			// Game over
			if (Player.Instance.Life <= 0) {
				DestroyObject (gameObject);
			}
		}
	}
}
